package com.csvproject;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CSVReader {

    private Integer bought = 0;
    private Integer supplied = 0;
    private Integer result = 0;
    private List<String> content = new ArrayList<>();
    private File baseFile;

    public void readFile(File file) throws IOException {
        this.baseFile = file;
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = reader.readLine()) != null){
             content.add(line);
        }
    }

    private void processList(){
        String type;
        Integer amount;
        Integer totalBought = 0;
        Integer totalSupplied = 0;
        for(String line : content) {
            type = line.substring(0, line.indexOf(','));
            amount = Integer.parseInt(line.substring(line.indexOf(',') + 1));
            if (type.equals("buy")) {
                totalBought += amount;
            } else if (type.equals("supply")) {
                totalSupplied += amount;
            }
        }
        bought = totalBought;
        supplied = totalSupplied;
    }

    public File createRecord() throws IOException{
        processList();
        String path = baseFile.getAbsolutePath();
        File file = new File(path.substring(0, path.lastIndexOf('\\')) + 
                "processed" +baseFile.getName());
        if(!file.exists()){
            file.createNewFile();
        }
        result = supplied - bought;
        FileWriter writer = new FileWriter(file);
        writer.write("supplied," + supplied + "\n");
        writer.write("bought," + bought + "\n");
        writer.write("result," + result);
        writer.close();
        return file;
    }

    //Solely for testing purposes
    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }

    public void setBaseFile(File baseFile) {
        this.baseFile = baseFile;
    }
}
