package com.csvproject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CSVReaderTest {

    File file;
    
    @BeforeEach
    void createTestFile(){
        String fileName = "testFile.csv";
        file = new File("C:\\Users\\wojte\\IdeaProjects" +
                "\\CSVProcessor\\src\\main\\resources\\" + fileName);
        try {
            if(!file.exists()){
                file.createNewFile();
            }
            FileWriter writer = new FileWriter(file);
            writer.write("buy," + 10 + "\n");
            writer.write("buy," + 5 + "\n");
            writer.write("supply," + 100 + "\n");
            writer.write("supply," + 145 + "\n");
            writer.write("buy," + 5 + "\n");
            writer.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    @Test
    void shouldReadFile(){
        String expected = "[buy,10, buy,5, supply,100, supply,145, buy,5]";
        CSVReader reader = new CSVReader();
        try{
            reader.readFile(file);
        }catch (IOException e){
            System.out.println("Something went wrong");
        }

        assertEquals(expected, reader.getContent().toString());
    }

    @Test
    void shouldProcessResultAndWriteToFile(){
        List<String> processedContent = new ArrayList<>();
        String line;
        CSVReader reader = new CSVReader();
        List<String> testContent = new ArrayList<>();
        testContent.add("supply,40");
        testContent.add("buy,20");
        testContent.add("supply,50");
        testContent.add("buy,10");
        testContent.add("buy,5");
        testContent.add("supply,100");
        reader.setContent(testContent);
        reader.setBaseFile(file);
        try{
            BufferedReader reader1 = new BufferedReader(new FileReader(reader.createRecord()));
            while ((line = reader1.readLine()) != null){
                processedContent.add(line);
            }
        }catch (IOException e){
            System.out.println("Something went wrong");
        }

        assertEquals("[supplied,190, bought,35, result,155]", processedContent.toString());

    }
}